from sqlalchemy import Boolean, Column, Integer, String, DateTime
from database import Base


class Vehicles(Base):
    __tablename__ = "vehicles"

    id = Column(Integer, primary_key=True, index=True)
    registration_number = Column(String)
    arrival = Column(DateTime)
    departure = Column(DateTime)
    duration = Column(Integer)
    status = Column(Boolean)
    bill = Column(Integer)


class Rules(Base):
    __tablename__ = "rules"

    id = Column(Integer, primary_key=True, index=True)
    capacity = Column(Integer)
    bill1 = Column(Integer)
    bill2 = Column(Integer)

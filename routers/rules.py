import sys
from datetime import datetime

from starlette import status
from starlette.responses import RedirectResponse

from fastapi import Depends, APIRouter, Request, Form, BackgroundTasks
import models
from database import engine, SessionLocal
from sqlalchemy.orm import Session

from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from celery import Celery

router = APIRouter(
    prefix="/rules",
    tags=["rules"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)

templates = Jinja2Templates(directory="templates")


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@router.get("/edit-rules", status_code=status.HTTP_200_OK)
async def read_rules(request: Request, db: Session = Depends(get_db)):

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    count = db.query(models.Rules).count()

    if count <= 0:
        rules_model = models.Rules()
        rules_model.id = 1
        rules_model.capacity = 10
        rules_model.bill1 = 5000
        rules_model.bill2 = 3000

        db.add(rules_model)
        db.commit()

    # return db.query(models.Rules).all()
    return templates.TemplateResponse("rules.html", {"request": request, "rules": rules})


@router.post("/edit-rules", status_code=status.HTTP_201_CREATED)
async def edit_rules(request: Request,
                     capacity: int = Form(...),
                     bill1: int = Form(...),
                     bill2: int = Form(...),
                     db: Session = Depends(get_db)):

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    rules.capacity = capacity
    rules.bill1 = bill1
    rules.bill2 = bill2

    db.add(rules)
    db.commit()

    return RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)

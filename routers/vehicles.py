import sys
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from starlette import status
from starlette.responses import RedirectResponse

from fastapi import Depends, APIRouter, Request, Form, BackgroundTasks
import models
from database import engine, SessionLocal
from sqlalchemy.orm import Session

from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from celery import Celery

router = APIRouter(
    prefix="/vehicles",
    tags=["vehicles"],
    responses={404: {"description": "Not found"}}
)

models.Base.metadata.create_all(bind=engine)

templates = Jinja2Templates(directory="templates")


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@router.get("/", status_code=status.HTTP_200_OK)
async def read_all_vehicle(request: Request, db: Session = Depends(get_db)):

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    count = db.query(models.Rules).count()

    if count <= 0:
        rules_model = models.Rules()
        rules_model.id = 1
        rules_model.capacity = 10
        rules_model.bill1 = 5000
        rules_model.bill2 = 3000

        db.add(rules_model)
        db.commit()

        bill1 = 5000
        bill2 = 3000
    else:
        bill1 = rules.bill1
        bill2 = rules.bill2

    vehicles = db.query(models.Vehicles).order_by(models.Vehicles.status.desc(), models.Vehicles.arrival.desc()).all()
    total_vehicles_true = 0

    for vehicle in vehicles:

        if vehicle.status:

            total_vehicles_true += 1
            current_time = datetime.now()
            time_difference = current_time - vehicle.arrival
            hours_difference = int(time_difference.total_seconds() / 3600)

            if hours_difference == 1:
                bill = bill1
            elif hours_difference > 1:
                bill = bill1 + (hours_difference - 1) * bill2
            else:
                bill = 0

            vehicle_model = db.query(models.Vehicles).filter(models.Vehicles.id == vehicle.id).first()
            vehicle_model.duration = hours_difference
            vehicle_model.bill = bill

            db.add(vehicle_model)
            db.commit()

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    capacity = rules.capacity - total_vehicles_true

    return templates.TemplateResponse("home.html", {"request": request, "vehicles": vehicles, "capacity": capacity})


@router.post("/add-vehicle", status_code=status.HTTP_201_CREATED)
async def add_vehicle(request: Request, registration_number: str = Form(...), db: Session = Depends(get_db)):

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    bill1 = rules.bill1
    bill2 = rules.bill2

    arrival_time = datetime.now()
    current_time = datetime.now()

    duration_parking = current_time - arrival_time
    hours_difference = int(duration_parking.total_seconds() / 3600)

    if hours_difference == 1:
        bill = bill1
    elif hours_difference > 1:
        bill = bill1 + (hours_difference - 1) * bill2
    else:
        bill = 0

    vehicle_model = models.Vehicles()

    vehicle_model.registration_number = registration_number
    vehicle_model.arrival = arrival_time
    vehicle_model.duration = hours_difference
    vehicle_model.status = True
    vehicle_model.bill = bill

    db.add(vehicle_model)
    db.commit()

    return RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)


@router.get("/complete/{vehicle_id}", response_class=HTMLResponse)
async def complete_vehicle(request: Request, vehicle_id: int, db: Session = Depends(get_db)):

    rules = db.query(models.Rules).filter(models.Rules.id == 1).first()
    bill1 = rules.bill1
    bill2 = rules.bill2

    vehicle = db.query(models.Vehicles).filter(models.Vehicles.id == vehicle_id).first()

    arrival = vehicle.arrival
    departure = datetime.now()

    duration_parking = departure - arrival
    hours_difference = int(duration_parking.total_seconds() / 3600)

    if hours_difference == 1:
        bill = bill1
    elif hours_difference > 1:
        bill = bill1 + (hours_difference - 1) * bill2
    else:
        bill = 0

    vehicle.status = False
    vehicle.departure = departure
    vehicle.duration = hours_difference
    vehicle.bill = bill

    db.add(vehicle)
    db.commit()

    return RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)


@router.get("/summary/{vehicle_id}", response_class=HTMLResponse)
async def summary_vehicle(request: Request, vehicle_id: int, db: Session = Depends(get_db)):
    vehicle = db.query(models.Vehicles).filter(models.Vehicles.id == vehicle_id).first()

    return templates.TemplateResponse("summary.html", {"request": request, "vehicle": vehicle})


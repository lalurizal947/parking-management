from fastapi import FastAPI, Depends
import models
from database import engine
from routers import vehicles, rules
from starlette.staticfiles import StaticFiles
from starlette import status
from starlette.responses import RedirectResponse


app = FastAPI()

models.Base.metadata.create_all(bind=engine)

app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/")
async def root():
    return RedirectResponse(url="/vehicles", status_code=status.HTTP_302_FOUND)


app.include_router(vehicles.router)
app.include_router(rules.router)
